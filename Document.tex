\documentclass[a4paper]{article}
\usepackage[pdftex]{graphicx}
\usepackage{amsmath}
\usepackage[normalem]{ulem}
\usepackage[margin=2cm,top=2cm]{geometry}
\usepackage{multicol}
\usepackage{fancyhdr}
\usepackage{color}
\usepackage{textcomp}
\usepackage{enumitem}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{draftwatermark}

\usepackage{hyperref}
\hypersetup{hyperindex=true,hyperfootnotes=true,linktocpage=true,colorlinks=true,linkcolor=blue,urlcolor=red,citecolor=magenta}

\title{Improving DNS Service Record (SRV) weights in sks-keyservers.net}
\author{Kristian Fiskerstrand}

\pagestyle{fancyplain}
\fancyhead{}
\fancyfoot{}
\fancyfoot[C]{\thepage}
\fancyfoot[L]{\scriptsize kf/\input{hgglobid.txt}/\input{hgid.txt}}
\renewcommand{\headrulewidth}{0pt}
\setlength{\columnsep}{30pt}
\begin{document}
\twocolumn[\maketitle]


\section{Introduction}
\url{http://sks-keyservers.net} provide a convenient way for end users of the security framework OpenPGP\cite{openpgp} to access synchronised and responsive HKP\cite{hkp} keyservers, mainly based on the software SKS\cite{sks}. A pool of keyservers is available at hkp://pool.sks-keyservers.net in addition to various sub-pools\cite{sks:overview}. This article will describe the implementation of DNS Service Records (\textbf{SRV}) weights as currently found in the sub-pools \textit{eu.pool} and \textit{na.pool} as a basis for a discussion and propose a better suited algorithm and implementation for determining the SRV weights. 

SRV provide a mechanism for directing more of the traffic to specific servers and is supported in GnuPG\cite{gnupg} since versions 1.4.10 and 2.0.13\cite{gnupg:srv}, whereby it is currently not supported by PGP\cite{pgp,pgp:srv}. Clients not supporting SRV weights directly will, however, still benefit as these are used as selection criteria for which of the servers are included with regular A and AAAA DNS records in the sub-pools. 

\section{Current implementation}
Currently the SRV weights are based on a simple network timing, where that status page\cite{sks:statuspage} of a given SKS keyserver is being downloaded, and the full calculation is being performed in \textsc{sks\_get\_peer\_data.php} \cite{pool:getpeerdata}. The SRV weight is calculated as:
\begin{equation}
\label{currentmethod}
\text{weight} = (\text{\textbf{int}})\left(\frac{100}{\text{responsetime}}\right) 
\end{equation}

Responsetime is defined (calculated) in \cite[lines~102--115]{pool:getpeerdata} and a local adjustment is applied to any SKS server on the local network of the pool server\cite[lines~119--122]{pool:getpeerdata}. The SKS servers are then sorted in descending order by their weights, and the top 10 servers are included in the respective sub-pool. The SRV weights are calculated on each full update run of the pool, currently running every hour. 

\subsection{Advantages}
The advantage of eq.~\eqref{currentmethod} is that the calculation is transparent and easily constrained within the server discovery process. The resulting output values perform reasonably well, and a SRV capable client is performing most of the work in determining which server to direct the traffic to. 

\subsection{Disadvantages}
Only a single measurement is performed at the time of server discovery, and the performance measurements as a client is only performed from a single point\footnote{The location of the server} for each of the pools\footnote{Two mirrors exists, one in Norway that is used for the EU pool and one in USA that is used for the North America pool}. In addition the size of the SKS status page is typically less than 3 KiB, which doesn't provide enough information for a proper measurement of the bandwidth capacity of the server. Other factors, such as the ability to serve multiple requests e.g. by using a reverse proxy in front of SKS to counter that \textit{``a bandwidth-constrained client is capable of executing this DoS attack inadvertently''} are not considered\footnote{Although this can be filtered separately similar to the High-Availibility pool ha.pool}.\cite{sks-devel:dos}

\section{Proposal for new implementation}
A new implementation ideally consider
\begin{enumerate}[label={\bfseries (\Roman*)},labelindent=20pt,labelwidth=20pt,leftmargin=!]
	\item The response time (\textbf{R})\label{enum:new_proposal:responsetime}
	\item The bandwidth capacity (\textbf{B})\label{enum:new_proposal:bandwidth}
	\item That the server is running behind a reverse proxy so that it can handle multiple requests (\textbf{P})\label{enum:new_proposal:responsiveness}
\end{enumerate}

The measurement for $R$ is done in seconds, with millisecond precision. Multiple clients should be used for these measurements in order to avoid the problem of only one client measurement in the current implementation. A PHP client\cite{sks:phpclient} and a Perl/CGI client\cite{sks:perlclient} are already available, and it can easily be ported to other platforms. The clients can be run by multiple users at various locations, and are accessible to the server performing the update run, to get timings for downloading a specific OpenPGP key from various servers. 

To get more stable result metrics over time some smoothing can be performed, e.g. for R in the form 
\begin{equation}
R = \frac{\sum\limits_{j=1}^{C}\sum\limits_{i=0}^{n-1} (1-\frac{i}{n})R_{ji}}{\sum\limits_{j=1}^{C}\sum\limits_{i=0}^{n-1} 1-\frac{i}{n}}
\end{equation}
where $R_{ji}$ define responsetime in current and earlier measurements from the various clients, $n$ is the number of entries to take into account, $C$ is the number of clients to use in the update run, and $R$ constitute the basis for further calculations. 

The use of these measurements should not be strictly linear but penalize slower servers based on an exponential factor of some form, e.g. $(x - z)^y$ where $z$ is defined as a left-skew of the mean ($\mu$), as determined based on an $N(\sigma)$ rejection criteria, in order to remove outliers from the calculation, and $y$ is a static constant (even number) defined previous to implementation. As the calculation is dependant on data collected about all the servers, this should be implemented in \textsc{sks-status.php}\cite{pool:sks-status}. Only servers that are considered to be part of the main pool\footnote{Needs to be responsive, updated with appropriate number of keys and proper SKS version} will be considered. 

The bandwidth information could be collected from the server operators e.g. in the form Megabit (or Gigabit) per seconds. 

The process could then be described as:
\begin{enumerate}[label={\bfseries (\arabic*)},labelindent=20pt,labelwidth=20pt,leftmargin=!]
	\item Gather bandwidth information from the operators
	\item During an update run, determine \ref{enum:new_proposal:responsetime} and \ref{enum:new_proposal:responsiveness} for each individual server.
	\item Calculate the mean ($\mu_R$) and standard deviation ($\sigma_R$) of \ref{enum:new_proposal:responsetime} across the servers in the pool.
	\item Exclude outliers to the results based on $N(\sigma)$ rejection criteria.
	\item Calculate new $\mu$ and $\sigma$ for the remainder of the servers. 
	\item Calculate the SRV weights for the individual servers.
\end{enumerate}

SRV weights would be defined as: 
\begin{equation}
\begin{split}
\text{weight} = \alpha \\ 
&+ \beta_R \left( \frac{100}{(R - \left(\mu_R - 2\sigma_R)\right)^y} \right) \\
\\
&+ \beta_B \qquad \left(\frac{B}{\sum\limits_{j=1}^{m} B_j}\right) \\
\\
&+ \beta_P \cdot D_P \cdot \rho
\end{split}
\end{equation}

\textbf{where}:
\begin{itemize}
\item $\alpha$ is a constant to provide a base weight
\item $\beta_x$ is the loading (weight) of the respective factor in determining the SRV weight
\item $B$ is the bandwidth information collected (Mbit/s) on the individual server and $B_j$ refer to the bandwidth capacity of other servers viable for inclusion (total number of $m$)
\item $D_P$ is a dummy variable that is 1 if a reverse proxy exists and 0 otherwise
\item $\rho$ is the additional weight given for a reverse proxy enabled server (presuming $\beta_P = 1$, or otherwise scaled in accordance to this variable)
\item $y$ is a constant (even number) that define penalization for deviation
\end{itemize}

As deviations in $R$ are penalized in both directions, we left-skew $\mu_R$ by two $\sigma_R$ in order to give higher weights to more responsive servers. A subset of available clients for measuring response-time should be selected based on the geographical region of the sub-pool. 

The SRV weights are then calculated, converted to an integer value, and the servers are sorted by descending order of weights, whereby the top 10 servers are added to the pool. 

{
\footnotesize
\begin{thebibliography}{99}
\bibitem{gnupg} \url{http://gnupg.org}
\bibitem{pool:getpeerdata} \url{http://code.google.com/p/sks-keyservers-pool/source/browse/trunk/sks-keyservers.net/status-srv/sks_get_peer_data.php?spec=svn101&r=100}
\bibitem{pool:sks-status} \url{http://code.google.com/p/sks-keyservers-pool/source/browse/trunk/sks-keyservers.net/status-srv/sks-status.inc.php?spec=svn101&r=100}
\bibitem{sks:statuspage} \url{http://keys.kfwebs.net:11371/pks/lookup?op=stats}
\bibitem{sks:overview} \url{http://sks-keyservers.net/overview-of-pools.php}
\bibitem{sks-devel:dos} \url{http://lists.gnu.org/archive/html/sks-devel/2012-03/msg00006.html}
\bibitem{sks:phpclient} \url{http://code.google.com/p/sks-keyservers-pool/source/browse/trunk/sks-keyservers.net/clients/key_retrieval.php}
\bibitem{sks:perlclient} \url{http://code.google.com/p/sks-keyservers-pool/source/browse/trunk/sks-keyservers.net/clients/key_retrieval.pl}
\bibitem{sks} \url{http://code.google.com/p/sks-keyserver/}
\bibitem{openpgp} \url{http://tools.ietf.org/html/rfc4880}
\bibitem{hkp} \url{http://tools.ietf.org/html/draft-shaw-openpgp-hkp-00}
\bibitem{gnupg:srv} \url{http://lists.gnu.org/archive/html/sks-devel/2010-04/msg00013.html}
\bibitem{pgp:srv} \url{http://www.symantec.com/connect/forums/dns-service-records-srv}
\bibitem{pgp} \url{http://www.symantec.com/theme.jsp?themeid=pgp}
\end{thebibliography}
}
\end{document}
